# Controller der abgesichert ist
class SecuredController < ApplicationController
  before_action :authenticate_user!
end
