class MoviesController < SecuredController
  before_action :set_movie, only: [:show]

  # GET /movies
  def index
    @movies = Movie.all

    render json: @movies
  end

  # GET /movies/1
  def show
    render json: @movie
  end

  def search
    @movies = Movie.search(params[:search_value])

    render json: @movies
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_movie
    @movie = Movie.find(params[:id])
  end
end
