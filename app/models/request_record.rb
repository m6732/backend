require 'uri'

class RequestRecord
  include ActiveModel::AttributeMethods

  def initialize(args = {})
    self.initialize_attributes(args)
  end

  class << self
    def all
      endpoint = 'movie/popular'
      data = client.execute(endpoint)
    end

    def find(id)
      endpoint = 'movie/'
      data = client.show(endpoint, id)
    end


    def search(search_value)
      endpoint = 'search/movie'
      search_params = '&query=' + URI.encode(search_value)
      data = client.search(endpoint, search_params)
    end

    private

    # Request Client erstellen
    def client
      RequestClient.new()
    end
  end

  protected

  attr_accessor :endpoint
  attr_accessor :params

  def initialize_attributes(args = {})
    args.each do |k, v|
      begin
        self.send("#{k}=", v)
      rescue Exception => ex
      end
    end
  end
end
