class RequestClient
  DEFAULT_URL = 'https://api.themoviedb.org/3/'.freeze
  API_KEY = '?api_key=336085bc89f2531aebd8a62fc3b399e6&language=de-DE'.freeze

  def initialize(args = {})
    #self.url = API_KEY + LANG
    self.movie_id = args[:search_value]
    self.search_value = args[:search_value]
  end

  def execute(endpoint = '')
    response = RestClient.get(DEFAULT_URL + endpoint + API_KEY)
    JSON.parse(response)
  end

  def show(endpoint = '', params = '')
    response = RestClient.get(DEFAULT_URL + endpoint + params + API_KEY)
    JSON.parse(response)
  end

  def search(endpoint, search_params = '')
    response = RestClient.get(DEFAULT_URL + endpoint + API_KEY + search_params)
    JSON.parse(response)
  end

  protected

  attr_accessor :url
  attr_accessor :movie_id
  attr_accessor :search_value
end
