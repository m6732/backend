# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  allow_password_change  :boolean          default(FALSE)
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  name                   :string
#  nickname               :string
#  firstname              :string
#  lastname               :string
#  image                  :string
#  email                  :string
#  tokens                 :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  language               :string           default("de")
#  security_role_id       :bigint(8)
#

FactoryBot.define do
  factory :user do
    email { 'jlampe97@web.de' }
    firstname { 'Jannes' }
    lastname { 'Lampe' }
    password { 'topsecret' }
    password_confirmation { 'topsecret' }
  end

  factory :invalid_user, parent: :user do
    email { 'invalid@muench-its.de' }
    password { 'topsecret' }
    password_confirmation { 'falsch' }
  end

  factory :user2, class: User do
    email { "testuser2@muench-its.de" }
    name { "Tester" }
    nickname { "Mister test" }
    password { "JustTestAround" }
  end
end
