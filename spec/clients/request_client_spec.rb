require 'rails_helper'

describe RequestClient do
  describe 'execute function' do
    it 'should return some data' do
      response = RequestClient.new.execute('movie/popular')
      expect(response).not_to be nil
      expect(response.class).to be Hash
    end
  end
end
